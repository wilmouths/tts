/*
 * Javascript library for the creation of a texture triangle to GEPPA standards
 * Library developped for projet with INRA-ASTER - MIRECOURT
 * Author: WILMOUTH Steven
 * Version: 1.0
*/

var TTS = {
	name: "TTS",
	build: function(params) {
		var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
		$(svg).attr('id', 'tts')
		$(svg).attr('height', (params.height !== undefined) ? params.height : 600)
		$(svg).attr('width', (params.width !== undefined) ? params.width : 600)
		svg.setAttribute('viewBox', '0 0 1200 1080')

		this.buildFigure('polygon', { 
			points: '100 0, 1100 1000, 100 1000',
			stroke: 'black',
			'stroke-width': 1,
			fill: 'black'
		}, svg)

		this.buildElements(params, svg)
		this.buildAxis(params, svg)
		this.buildAxisName(params, svg)

		this.popup(params)

		$(params.container).append(svg)
		var svg = document.querySelector('svg')
	},
	buildFigure: function(type, params, svg) {
		var figure = document.createElementNS("http://www.w3.org/2000/svg", type)
		$.each(params, function(k, v) {
			$(figure).attr(k, v)
		})
		$(figure).on('mouseenter', function(e) {
			this.style.opacity = 0.8

			var x = e.clientX
			var y = e.clientY

			var popupHeight = $("#popupTTS").height();
			var popupWidth = $("#popupTTS").width();

			$('#popupTTS').css('top', (y - (popupHeight / 2) + 30))
			$('#popupTTS').css('left', (x - (popupWidth / 2)))
			$('#popupTTS').text($(this).attr('data-name'))

			$('#popupTTS').fadeIn('slow')
		})
		$(figure).on('mouseleave', function(e) {
			this.style.opacity = 1
			$('#popupTTS').fadeOut('slow')
		})

 		$(svg).append(figure)
	},
	buildElement: function(params, svg) {
		this.buildFigure('polygon', params.figure, svg)
		this.buildText(params.text, svg)
	},
	buildText: function(params, svg) {
		var text = document.createElementNS("http://www.w3.org/2000/svg", 'text')
		$.each(params, function(k, v) {
			if (k === 'text') {
				$(text).text(v)
			} else {
				$(text).attr(k, v)
			}
		})
		$(svg).append(text)
	},
	buildElements: function(params, svg) {
		this.heacyClays(params, svg)
		this.clays(params, svg)
		this.sandyClays(params, svg)
		this.claysLoams(params, svg)
		this.verySandyClays(params, svg)
		this.claysSandySlit(params, svg)
		this.clayLoam(params, svg)
		this.claySands(params, svg)
		this.claySiltySands(params, svg)
		this.sandClayLoams(params, svg)
		this.silts(params, svg)
		this.sands(params, svg)
		this.sandsLoams(params, svg)
		this.sandyLoams(params, svg)
		this.pureSilt(params, svg)
		this.pureSands(params, svg)
		this.toto(params, svg)
	},
	toto: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles limono-sableuse',
				points: '300 572, 550 600, 636 714, 338 692',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 430,
				y: 660,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Als'
			}
		}, svg)
	},
	heacyClays: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles lourdes',
				points: '100 0, 560 460, 560 460, 100 400',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 250,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'AA'
			}
		}, svg)
	},
	clays: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles',
				points: '100 400, 560 460, 720 620, 100 550',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 485,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'A'
			}
		}, svg)
	},
	sandyClays: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles sableuses',
				points: '100 550, 300 572, 340 692, 100 674',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 630,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'As'
			}
		}, svg)
	},
	claysLoams: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles limoneuses',
				points: '550 600, 720 620, 830 730, 636 715',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 670,
				y: 675,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Al'
			}
		}, svg)
	},
	verySandyClays: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Argiles très sableuses',
				points: '100 674, 338 692, 350 730, 350 790, 100 775',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 740,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'AS'
			}
		}, svg)
	},
	claysSandySlit: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons argilo-sableux',
				points: '338 692, 636 714, 710 812, 350 790, 350 730',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 475,
				y: 765,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'LAS'
			}
		}, svg)
	},
	clayLoam: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons argileux',
				points: '636 714, 830 730, 925 825, 708 812',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 760,
				y: 780,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'La'
			}
		}, svg)
	},
	claySands: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Sables argileux',
				points: '100 775, 350 790, 350 886, 100 875',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 840,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Sa'
			}
		}, svg)
	},
	claySiltySands: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Sables arilo-limoneux',
				points: '350 790, 440 796, 490 892, 350 886',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 380,
				y: 855,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Sal'
			}
		}, svg)
	},
	sandClayLoams: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons sablo-argileux',
				points: '438 796, 708 812, 778 904, 488 892',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 575,
				y: 860,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Lsa'
			}
		}, svg)
	},
	silts: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons',
				points: '708 812, 925 825, 1014 914, 778 904',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 850,
				y: 875,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'L'
			}
		}, svg)
	},
	sands: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Sables',
				points: '100 875, 350 886, 350 986, 100 925',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 915,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'S'
			}
		}, svg)
	},
	sandsLoams: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Sables limoneux',
				points: '350 886, 488 892, 544 1000, 350 1000',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 410,
				y: 960,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Sl'
			}
		}, svg)
	},
	sandyLoams: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons sableux',
				points: '488 892, 778 904, 850 1000, 544 1000',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 640,
				y: 960,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'Ls'
			}
		}, svg)
	},
	pureSilt: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Limons purs',
				points: '778 904, 1014 914, 1100 1000, 850 1000',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 910,
				y: 960,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'LL'
			}
		}, svg)
	},
	pureSands: function(params, svg) {
		this.buildElement({
			figure: { 
				'data-name': 'Sables purs',
				points: '100 925, 350 984, 350 1000, 100 1000',
				stroke: (params.stroke !== undefined) ? params.stroke : 'black',
				'stroke-width': (params.strokeWidth !== undefined) ? params.strokeWidth : 1,
				fill: (params.fill !== undefined) ? params.fill : '#eee',
			},
			text: {
				x: 110,
				y: 980,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: 'SS'
			}
		}, svg)
	},
	buildAxis: function(params, svg) {
		this.xAxis(params, svg)
		this.yAxis(params, svg)
	},
	xAxis: function(params, svg) {
		this.buildText({
			x: 66,
			y: 1008,
			'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: '0 -'
		}, svg)

		for (var i = 9; i >= 1; i--) {
			this.buildText({
				x: 32,
				y: (1000 - (i * 100)),
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: (i * 100) + ' -'
			}, svg)
		}

		this.buildText({
			x: 15,
			y: 8,
			'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: '1000 -'
		}, svg)
	},
	yAxis: function(params, svg) {
		this.buildText({
			x: 92,
			y: 1040,
			'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: '0'
		}, svg)
		this.buildFigure('line', {
			x1: 100,
			y1: 1000,
			x2: 100,
			y2: 1008,
			stroke: params.stroke,
			'stroke-width': 2
		}, svg)

		for (var i = 1; i < 11; i++) {
			this.buildText({
				x: ((i === 10) ? 1064 : (72 + (i * 100))),
				y: 1040,
				'font-size' : (params.fontSize !== undefined) ? params.fontSize : '30',
				fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
				text: (i * 100)
			}, svg)
			this.buildFigure('line', {
				x1: (i * 100),
				y1: 1000,
				x2: (i * 100),
				y2: 1008,
				stroke: params.stroke,
				'stroke-width': 2
			}, svg)
		}

		this.buildFigure('line', {
			x1: 1100,
			y1: 1000,
			x2: 1100,
			y2: 1008,
			stroke: params.stroke,
			'stroke-width': 2
		}, svg)
	},
	buildAxisName: function(params, svg) {
		this.buildText({
			x: -680,
			y: 18,
			'font-size' : 24,
			'font-weight': 'bold',
			transform: 'rotate(270, 0, 0)',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: 'Argiles (inf. à 0,002 mm) ‰'
		}, svg)

		this.buildText({
			x: 610,
			y: -100,
			'font-size' : 24,
			'font-weight': 'bold',
			transform: 'rotate(45)',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: 'Sables (de 0,05 à 2 mm) ‰'
		}, svg)

		this.buildText({
			x: 380,
			y: 1080,
			'font-size' : 24,
			'font-weight': 'bold',
			fill: (params.fontColor !== undefined) ? params.fontColor : '#eee',
			text: 'Limons (de 0,002 à 0,05 mm) ‰'
		}, svg)
	},
	popup: function(params) {
		var popupStyle = {
			position: 'absolute',
			top: 50,
			left: 50,
			padding: '10px',
			'background-color': 'rgba(0, 0, 0, 0.8)',
			color: '#fff',
			'border-radius': '5px'
		}
		var popupTTS = document.createElement('div')
		var style = 'display: none;'
		$.each(popupStyle, function(k, v) {
			style = (style + k + ': ' + v + ';')
		})
		$(popupTTS).attr('style', style)
		$(popupTTS).attr('id', 'popupTTS')
		document.body.append(popupTTS)
	},
	addPoint: function(x, y) {
		y = (992 - y)
		x = (100 + x)
		var lineX = document.createElementNS('http://www.w3.org/2000/svg', 'line')
		$(lineX).attr('x1', '101')
		$(lineX).attr('y1', y)
		$(lineX).attr('x2', x)
		$(lineX).attr('y2', y)
		$(lineX).attr('stroke', '#5184AF')
		$(lineX).attr('stroke-width', '5')
		$(lineX).attr('stroke-linecap', 'square')
		$(lineX).attr('stroke-dasharray', '1, 10')
		document.querySelector('#tts').appendChild(lineX)

		var lineY = document.createElementNS('http://www.w3.org/2000/svg', 'line')
		$(lineY).attr('x1', x)
		$(lineY).attr('y1', y)
		$(lineY).attr('x2', x)
		$(lineY).attr('y2', '1000')
		$(lineY).attr('stroke', '#5184AF')
		$(lineY).attr('stroke-width', '5')
		$(lineY).attr('stroke-linecap', 'square')
		$(lineY).attr('stroke-dasharray', '1, 10')
		document.querySelector('#tts').appendChild(lineY)

		var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle')
		$(circle).attr('cx', x)
		$(circle).attr('cy', y)
		$(circle).attr('r', '10')
		$(circle).attr('stroke', 'blue')
		$(circle).attr('stroke-width', '2')
		$(circle).attr('fill', 'white')
		circle.addEventListener('click', function() {
			alert('ddd')
		})
		document.querySelector('#tts').appendChild(circle)
	}
}