# TTS
Javascript library for the creation of a texture triangle to GEPPA standards

## Version
1.0.0

## Usage
### Prerequisites
You need linked jQuery in your project

### Initialization
```js
TTS.build({
  container: '#svgTTS',
  width: 700,
  height: 700,
  stroke: 'black',
  strokeWidth: 1,
  fill: '#eee',
  fontColor: 'black'
})
```

```html
<div id="svgTTS"></div>
```

## Adding point in figure
```js
TTS.addPoint(x, y)
```

## Screen
![TTS](https://img4.hostingpics.net/pics/879941TTS.png)
